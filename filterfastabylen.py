#
# simple filter by length script, inputs are a fasta file and a 
# size threshold (exclusive). Output is printed to stdout.
#

from Bio import  SeqIO as sio
import sys

tot = 0
bad = 0

for s in sio.parse(sys.argv[1], "fasta"):
    tot += 1
    if len(s.seq) > int(sys.argv[2]) :
        print(">{}\n{}\n".format(s.id, str(s.seq)))

    else:
        bad += 1
        pass

print("{} sequences were discarded\n".format(bad), file=sys.stderr)

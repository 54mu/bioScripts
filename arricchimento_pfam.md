# pipeline per l'arricchimento di subset

## 1. hmmer del set intero in fasta contro PFAM

```
hmmsearch --domtblout domains.tbl --cpu 16 ~/Shared/Databases/pfam/Pfam-A.hmm Sbr_peptide_set.fa
```

## 2. Estrazione coppie nome-pfam

attenzione che non ci siano coppie ripetute!

```
sed 's/\s\{1,\}/\t/g' domains.tbl | grep -v ^# | cut -f1,5 | sort | uniq
```

## 3. Test vero e proprio

da ottimizzare lo script ma si comporta bene. vuole in ordine: coppie del passaggio precedente, lista del subset, dimensione totale del trascrittoma

```
python ~/Shared/PFAM_enrichment/test.py annotation_table.tsv subset (number of features in the genome/transcriptome)
```
crea un file con la parte iniziale del nome uguale a quella della lista subset

## 4. Associazione nomi a pfamid

estrazione delle corrispondenze dalla tabella del primo passaggio

```
sed 's/\s\{1,\}/\t/g' domains.tbl | grep -v ^# | cut -f4,5 |sort | uniq | awk '{print $2 "\t" $1}' > dict
```
join delle tabelle

```
join -j 1 -o 1.1 1.2 1.3 1.4 2.2 <(sort <(tail -n +2 Gene_Names_Fully_spanned_genes_enrichment_test.tsv)) <(sort dict) | sed 's/ /\t/g'  
```

## 5. eventuale estrazione di sequenze dal fasta iniziale in base a pfam

attenzione che non ci devono essere linee vuote nel fasta (toglierle con sed)

e se ci sono spazi nei nomi del fasta sono da troncare (sempre sed)

```
grep PF18644.1 domains.tbl | sed 's/\s\{2,\}/\t/' | cut -f1 | sort | uniq | xargs -n 1 samtools faidx Achatina_immaculata_peptideset.fa
```

# Arricchimento GO

## Workflow
+ diamond blastp contro uniprot con soglia evalue 1e-5
+ estrazione di corrispondenza tra sequenza e uniprotID
+ estrazione di corrispondenza tra uniprotID e GO (con un GO e un uniprotID per riga! (gestionde id GO multipli))
+ associazione sequenza - GO
+ arricchimento

## Diamond

### creazione database (da aggiornamento uniprotsprot)

```
diamond makedb --in uniprot_sprot.fasta -d  uniprot_sprot.dmnd 
```

### aggiornamento tabella di annotazione


### comando [diamondo] 

```
diamond blastp -p 64 -d ~/Shared/Databases/uniprot/uniprot_sprot.dmnd -o afulica.dmntd.tbl -f 6 qseqid sseqid evalue -q Peptide_seq.fa -e 1e-5 -k 10 --sensitive
```

### fix tabella 

```
sed 's/|/\t/g' afulica.dmntd.tbl | cut -f 1,3 > seq2uniprot.tbl   
```
si tengono tutti gli hit o solo il migliore?

Tenere soltanto i 10 hit più significativi di diamond. 

### pipeline completa

```bash
#!/bin/bash

genome=$1
subset=$2

diamond blastp -p 64 -d ~/Shared/Databases/uniprot/uniprot_sprot.dmnd -o $genome.dmntd.tbl -f 6 qseqid sseqid evalue -q $genome -e 1e-5 -k 10 --sensitive

sed 's/|/\t/g' $genome.dmntd.tbl | cut -f 1,3 | sort | uniq > $genome.seq2uniprot.tbl

python ~/Shared/enrichment_pipeline/seq2go.py $genome.seq2uniprot.tbl $genome.seq2go.tbl

sort $genome.seq2go.tbl | uniq > $genome.seq2go.uniq.tbl

python ~/Shared/PFAM_enrichment/test.py $genome.seq2go.uniq.tbl $subset $(grep -c "^>" $genome) 

```
